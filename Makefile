#! /usr/bin/make -f
# makefile for collectd lvm plugin
# Author: Joseph Nahmias <joe@nahmias.net>
# License: GPL-2

INSTALL ?= /usr/bin/install
PREFIX ?= /usr

CFLAGS ?= -Wall -Werror -Wformat -Werror=format-security -g -O2

LDLIBS ?= -lyajl

lvm.so: lvm.o
	$(CC) -shared $(LDFLAGS) -Wl,-soname -Wl,$@ $^ $(LDLIBS) -o $@

lvm.o: lvm.c

clean:
	rm -rf lvm.o lvm.so

CONFDIR ?= $(DESTDIR)/etc/collectd/collectd.conf.d
PLUGINDIR ?= $(DESTDIR)$(PREFIX)/lib/collectd
install: lvm.so
	$(INSTALL) -d -o root -g root -m 0755 $(PLUGINDIR)
	$(INSTALL)    -o root -g root -m 0644 lvm.so $(PLUGINDIR)
	$(INSTALL)    -o root -g root -m 0644 collectd-lvm.conf $(CONFDIR)

.PHONY: clean install
